# Perfei_fly

Perfei_fly项目是本人基于其他开源飞控制作，在此基础上优化代码结构，加入了一些新功能，经测试，飞行效果良好；经过一段时间整理后，现拿出来分享给大家~。

## 飞机功能：

1、基础飞行功能。

2、支持匿名上位机显示无人机姿态数据。

3、支持匿名上位机读取飞控pid、写入飞控pid、恢复默认pid值。

4、数据回传功能。

## 遥控器功能：

1、基础遥控功能。

2、屏幕显示功能。

## 指南

[四轴无人机操作说明](./05_guide/guide.md)

[固定翼手抛机操作说明](./05_guide/guide2.md)

[学习分享](./05_guide/share.md)

[版本更新说明](./05_guide/release_notes.md)

## 相关链接

四轴无人机PCB工程开源地址：https://oshwhub.com/jiaLC_fei/Perfei_Fly1

四轴无人机B站视频地址：https://www.bilibili.com/video/BV1ju411e78F/

固定翼手抛机PCB工程开源地址：https://oshwhub.com/jialc_fei/pefeifly_v2-0

固定翼手抛机B站视频地址：https://www.bilibili.com/video/bv18P41137ST?

## 仓库目录结构

```
.
├── PerFei_V3.0(全功能版) //keil工程
├── PerfeiRemote_V3.0(全功能版) //keil工程
├── PerFei_V2.0 //飞控整个keil工程
├── Perfei_Remote //遥控器整个keil工程  
├── guide //相关说明文档
├── else //存放其他相关资料
	├── 3D //手抛机电机座3D打印文件
```