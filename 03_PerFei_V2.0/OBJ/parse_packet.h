#ifndef _parse_packet_h_
#define _parse_packet_h_

#include "nrf24l01.h"
#include "stm32f10x.h"

typedef enum{
	LOCK = 0,		
	UNLOCK,		
}Lock;//锁状态

typedef enum{
		SIGNAL_LOST = 0,
    SIGNAL_NORMAL,
}Signal;//信号状态

typedef enum{
		POWER_NORMAL = 0,
		POWER_LOWER,
}Power;//电量状态

typedef enum{
		PAIR_NOT = 0,
		PAIR_NORMAL,
}Pair;//配对状态

typedef struct
{
	uint16_t thr;		//油门
  uint16_t thr_zone;
	
	float pit;		//俯仰舵向
	float rol;		//横滚舵向
	float yaw;		//偏航舵向
  uint8_t key_l;		//左功能按键
  uint8_t key_r;		//右功能按键
    
  uint32_t key_l_cnt;
  uint32_t key_r_cnt;
    
  uint8_t key_l_flag  : 1;
  uint8_t key_r_flag  : 1;
	uint8_t mode        : 2;
	uint8_t high_flag   : 1;		//定高标志位
  
	uint8_t signalLostCount;		//信后丢失计数
	
	Lock lock;
	Signal signal;
	Power power;
	Pair pair;
	
	int16_t unlockCount ;
  uint16_t lockCount ;
	
	float voltage;
    
}PlaneData;



extern u8 Rx_packet[RX_PLOAD_WIDTH];

void nrf_parse_packet(void);
void parse_key_info(void);
void NrfACKPacket(void);


#endif

