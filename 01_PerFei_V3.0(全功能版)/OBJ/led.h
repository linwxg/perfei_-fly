#ifndef _led_h_
#define _led_h_

#include "stm32f10x.h"
#include "parse_packet.h"


#define		RGB_LED_HIGH	GPIOB->BSRR = GPIO_Pin_13;//����
#define 	RGB_LED_LOW		GPIOB->BRR = GPIO_Pin_13;//����
 
// #define	RGB_LED_HIGH	(GPIO_ResetBits(GPIOB, GPIO_Pin_14))
//#define 	RGB_LED_LOW		(GPIO_SetBits(GPIOB, GPIO_Pin_14))

typedef enum{
	RED = 1,	//��		
	GREEN,		//��
	BLUE,			//��
	YELLOW,		//��
	PURPLE,		//��
	CYAN,			//��
	WHITE,		//��
}LedColor;

void RGB_LedInit(void);
void RGB_LED_Write0(void);
void RGB_LED_Write1(void);
void RGB_LED_Reset(void);
void RGB_LED_Write_Byte(uint8_t byte);
void RGB_LED_Write_24Bits(uint8_t green,uint8_t red,uint8_t blue);
void RGB_LED_Red(void);
void RGB_LED_Green(void);
void RGB_LED_Blue	(void);
//void Ws2812Init(void);
//void ws2811_Reset(void);
//void ws281x_sendOne(uint32_t dat);
void RGB_LedStatus(const PlaneData plane);
void RGB_LedBlink(uint8_t ledColor);
#endif
//#define	TOP_RGB_RED			GPIOA->BRR = GPIO_Pin_12
//#define	TOP_RGB_GREEN		GPIOB->BRR = GPIO_Pin_14
//#define	TOP_RGB_BLUE		GPIOB->BRR = GPIO_Pin_13

//#define	BUTTOM_RGB_RED		GPIOB->BRR = GPIO_Pin_9;
//#define	BUTTOM_RGB_GREEN	GPIOB->BRR = GPIO_Pin_8;
//#define	BUTTOM_RGB_BLUE		GPIOB->BRR = GPIO_Pin_7;


//typedef enum{
//	RED = 1,	//��		
//	GREEN,		//��
//	BLUE,			//��
//	YELLOW,		//��
//	PURPLE,		//��
//	CYAN,			//��
//	WHITE,		//��
//}LedColor;


//void RGB_LedInit(void);

////#define led1 1
////#define led2 2
////#define led3 3
////#define led4 4

///*   LED2          LED1   */ 
//    /** *   /|\   * * *
//         *   |   *
//          *  |  *
//           * | *
//            * *
//             *
//            * *
//           *   *
//          *     *
//         *       *
//    * * *         * * */
///*  LED3            LED4   */ 



//void LeftButtomLedBlinkSet(void);
//void TopLedColorSet(uint8_t LedColor);
//void LedStatusOff(void);
//void RGB_LedStatus(PlaneData plane);
//void RGB_LedBlink(uint8_t ledColor);
//void ButtomLedColorSet(const uint8_t LedColor);
//#endif

