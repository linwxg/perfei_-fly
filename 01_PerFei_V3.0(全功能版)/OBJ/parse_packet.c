#include "parse_packet.h"
#include "nrf24l01.h"
#include "imath.h"
#include "controller.h"
#include "pair_freq.h"
#include "precompile.h"
u8 Rx_packet[RX_PLOAD_WIDTH] = {0};
u8 txPacket[TX_PLOAD_WIDTH] = {0};

PlaneData plane = {0};

//数据包解析
void nrf_parse_packet(void)
{
	if(NRF24L01_RxPacket(Rx_packet)==0){		//成功接收到数据
       if((Rx_packet[RX_PLOAD_WIDTH-1]!=0x8B)){
				 return;
			 }
        			 
		plane.signalLostCount = 0;
        
		plane.throttle = Rx_packet[2]<<8|Rx_packet[1];		//油门
			 
		#if FOUR_AXIS_UAV
			plane.pit = -(float)(((int8_t)Rx_packet[3]) - 50);		//俯仰舵向
			plane.rol = -(float)(((int8_t)Rx_packet[4]) - 50);		//横滚舵向
			plane.yaw =  (float)(((int8_t)Rx_packet[5]) - 50);		//偏航舵向
		#elif FIXED_WING_AIRCRAFT
//			plane.pit = -(float)(((int8_t)Rx_packet[3]) - 50);		//俯仰舵向
			plane.yaw =  (float)(((int8_t)Rx_packet[4]) - 50);		//横滚舵向
			plane.rol = -(float)(((int8_t)Rx_packet[5]) - 50);		//偏航舵向
		#elif BRUSHLESS_FOUR_AXIS_UAV
			plane.pit = -(uint16_t)(((Rx_packet[4]<<8|Rx_packet[3])) - 200);//俯仰舵向
			plane.rol = -(float)(((Rx_packet[6]<<8|Rx_packet[5])) - 200);//横滚舵向
			plane.yaw = -(float)(((Rx_packet[8]<<8|Rx_packet[7])) - 200);//偏航舵向	
		#endif
			 
    plane.key_l = Rx_packet[6];		//左功能按键
    plane.key_r = Rx_packet[7];		//右功能按键
		
		#if FOUR_AXIS_UAV
			plane.throttle = ThrottleLimit(plane.throttle,0,950);		//油门限制范围
		#elif FIXED_WING_AIRCRAFT
			plane.throttle = ThrottleLimit(plane.throttle,0,950);		//油门限制范围
		#elif BRUSHLESS_FOUR_AXIS_UAV
			plane.throttle = ThrottleLimit(plane.throttle,0,10000);		//油门限制范围
		#endif
		plane.throttle = direction_to_zero(plane.throttle,0,5);
    plane.pit = direction_to_zero(plane.pit,-5,5);		//消除抖动
    plane.rol = direction_to_zero(plane.rol,-5,5);		//
    plane.yaw = direction_to_zero(plane.yaw,-5,5);		//

//    plane.thr_zone = plane.throttle;//油门，用于定高控制
			 
	}else{
		if(plane.signalLostCount<200){		//信号丢失计数
				plane.signalLostCount++;  
				plane.signal = SIGNAL_NORMAL;
    }
		
		if(plane.signalLostCount==200){	
			plane.signal = SIGNAL_LOST;		//信号丢失
      if(plane.throttle>10){	
        plane.throttle -= 1;		//信号丢失减速处理                
        }
		}
		
	}
}


//向遥控器发送飞机数据
void NrfACKPacket(void)
{
	uint16_t temp = 0;
	txPacket[0] = 0xAA;
	txPacket[1] = plane.lock;
	txPacket[2] = plane.power;
	
	temp = (uint16_t)(plane.voltage * 100);
	txPacket[3] = *((u8*)&temp);
	txPacket[4] = *((u8*)&temp + 1);
	
	txPacket[TX_PLOAD_WIDTH - 1] = 0xAC;
	SPI_Write_Buf(W_ACK_PLOAD,txPacket,TX_PLOAD_WIDTH);
}
