#ifndef _parse_packet_h_
#define _parse_packet_h_

#include "nrf24l01.h"
#include "stm32f10x.h"

typedef enum{
	LOCK = 0,		
	UNLOCK,		
}Lock;//��״̬

typedef enum{
		SIGNAL_LOST = 0,
    SIGNAL_NORMAL,
}Signal;//�ź�״̬

typedef enum{
		POWER_NORMAL = 0,
		POWER_LOWER,
}Power;//����״̬

typedef enum{
	PAIR_NOT = 0,
	PAIR_NORMAL,
}Pair;//���״̬

typedef enum{
	DMP_NOT = 0,
	DMP_NORMAL,
}Dmp;

typedef struct
{
	uint16_t throttle;		//����
	float pit;		//��������
	float rol;		//�������
	float yaw;		//ƫ������
  uint8_t key_l;		//���ܰ�����δ���ƣ�
  uint8_t key_r;		//�ҹ��ܰ�����δ���ƣ�
	
	Lock lock;		//��״̬
	Signal signal;		//�ź�״̬
	Power power;	//����״̬
	Pair pair;		//���״̬
	Dmp dmp;		//dmp���ݻ�ȡ״̬
	
	uint16_t unlockCount ;
  uint16_t lockCount ;
	uint8_t signalLostCount;		//�ź�ʧ����
	
	float voltage;
    
}PlaneData;

extern u8 Rx_packet[RX_PLOAD_WIDTH];

void nrf_parse_packet(void);
void parse_key_info(void);
void NrfACKPacket(void);


#endif

