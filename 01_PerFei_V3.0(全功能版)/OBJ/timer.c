#include "timer.h"
#include "led.h"
#include "systick.h"
#include "iic.h"
#include "mpu6050.h"
#include "imu.h"
#include "usart2.h"
#include "nrf24l01.h"
#include "pwm.h"
#include "adc.h"
#include "controller.h"
#include "parse_packet.h"
#include "fc_status.h"
#include "gyro_cal.h"
#include "acc_cal.h"
#include "pair_freq.h"


extern PlaneData plane;
extern Mpu6050_Data Mpu;
extern Attitude att;

//定时器初始化 5ms
void TimerInit(void)
{ 
	TIM_TimeBaseInitTypeDef TIM_timeBaseStucture;
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3,ENABLE);
	
	TIM_timeBaseStucture.TIM_ClockDivision = TIM_CKD_DIV1;      	//分频因子，输出给定时器的ETRP数字滤波器提供时钟
	TIM_timeBaseStucture.TIM_Prescaler = 72-1;                		//预分频，给TIMx_CNT驱动的时钟，注意：实际的预分频值是0+1
	TIM_timeBaseStucture.TIM_Period = 5000-1;	
	TIM_timeBaseStucture.TIM_CounterMode = TIM_CounterMode_Up;		//向上计数
	TIM_TimeBaseInit(TIM3,&TIM_timeBaseStucture);
	
	TIM_Cmd(TIM3,ENABLE);
    
	TIM_ClearFlag(TIM3,TIM_FLAG_Update);							//先清除定时器更新标志位，防止一开启中断就进入中断处理函数中
	
	TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE);
}


uint32_t running_tim_cnt = 0;

//测试系统运行时间
void time_check(_Time_test *running)
{
    running->last_time_us = running->now_time_us;
    running->now_time_us = running_tim_cnt * 5000 + TIM3->CNT;
    
    running->delta_time_us = running->now_time_us - running->last_time_us;
    running->delta_time_ms = running->delta_time_us * 0.001f;
}

_Time_test run_start;
_Time_test run_stop;

void TIM3_IRQHandler(void)
{      
   	if(TIM3->SR&0X0001){        
			running_tim_cnt++ ;
			
			time_check(&run_start);
			
			//对频
			wait_pairing();		
	
			//数据回传
			NrfACKPacket();
			
			//无线数据解析
			nrf_parse_packet();

			//陀螺仪数据转换
			GyroDataTransformDeg(&Mpu.deg_s.x,&Mpu.deg_s.y,&Mpu.deg_s.z);
			
			//利用mpu6050 dmp获取欧拉角
			IMU(&att.rol,&att.pit,&att.yaw);
	
			//pid控制器
			ControModel();
			_controller_perform();
			ControllerOutput();
							
			//锁定状态轮询
			PlaneLockStatus();
			
			//指示灯装填轮询
			RGB_LedStatus(plane);
			
			//匿名地面站波形显示
			ANOSendStatus();
			
			
			time_check(&run_stop);		
			
	}				   
	TIM3->SR&=~(1<<0);  
}


